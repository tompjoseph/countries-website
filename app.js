// async function getData() {
//     const response = await fetch('https://restcountries.com/v3.1/all');
//     const countries = await response.json();
//     console.log(countries)
// }

// function displayData(countries) {
//     const countryContainer = document.getElementById('countryContainer')
//     const article = document.getElementById('countryData')
//     countries.forEach(country => {
//         const countryName = country.name.common
//         const population = country.population
//         const capital = country.capital

//         article.innerHTML = `<article id="countryData">
//                         <img src="./indian-flag.png" alt="">
//                         <h3>${countryName}</h3>
//                         <div class="details">
//                             <h4>Capital</h4>
//                             <p>${capital}</p>
//                         </div>
//                         <div class="details">
//                             <h4>Population</h4>
//                             <p>${population}</p>
//                         </div>
//                     </article>`
//     })
//     article.appendChild(countryContainer)
// }
// getData()

async function getData() {
    try {
        const response = await fetch('https://restcountries.com/v3.1/all');
        if (!response.ok) {
            throw new Error('Failed to fetch country data');
        }
        const countries = await response.json();
        displayData(countries);
    } catch (error) {
        console.error('Error fetching data:', error);
    }
}

function displayData(countries) {
    const countryContainer = document.getElementById('countryContainer');
    countryContainer.innerHTML = ''; // Clear previous content

    countries.forEach(country => {
        const countryName = country.name.common;
        const population = country.population.toLocaleString();
        const capital = country.capital?.[0] || 'N/A';
        const flag = country.flags.png;

        const article = document.createElement('article');
        article.classList.add('country');

        article.innerHTML = `
            <img src="${flag}" alt="${countryName} Flag">
            <h3>${countryName}</h3>
            <div class="details">
                <h4>Capital</h4>
                <p>${capital}</p>
            </div>
            <div class="details">
                <h4>Population</h4>
                <p>${population}</p>
            </div>
        `;

        countryContainer.appendChild(article);
    });
}

getData();